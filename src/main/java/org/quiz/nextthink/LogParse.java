package org.quiz.nextthink;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

public class LogParse {

    static final String ENGINE_START = "starting nxengine";
    static final String ENGINE_RUN = "nxengine is running";
    static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    static final int dateLen = format.toPattern().length(); //this work out in the specific case. in some cases pattern length might not equal date length;
    static final String path = "input/engine.log";

    public static void main(String[] args) {
        LogParse l = new LogParse();
        l.printDiff(path);
    }

    ArrayList<Instant> getTimes(String logPath) {

        ArrayList<Instant> times = new ArrayList<>();

        try (
                BufferedReader br = Files.newBufferedReader(Paths.get(logPath))
        ) {
            String current;
            while ((current = br.readLine()) != null) {
                if (current.contains(ENGINE_START) || current.contains(ENGINE_RUN)) {
                    String dateString = current.substring(0, dateLen);
                    times.add(format.parse(dateString).toInstant());
                }
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return times;
    }

    void printDiff(String path) {
        ArrayList<Instant> times = getTimes(path);

        for (int i = 0; i < times.size(); i += 2) {
            System.out.printf(
                    "Start Cycle: %d Duration: %s%n", 1 + i / 2, getDiffInHMS(times.get(i), times.get(i + 1))
            );
        }

    }

    String getDiffInHMS(Instant start, Instant end) {
        long diff = Duration.between(start, end).getSeconds();
        StringBuilder time = new StringBuilder();
        String hour = String.format("%02d", diff / 3600);
        diff %= 3600;
        String min = String.format("%02d", diff / 60);
        diff %= 60;
        String sec = String.format("%02d", diff);

        return time
                .append(hour)
                .append(":")
                .append(min)
                .append(":")
                .append(sec)
                .toString();
    }
}